/* Requerimientos : No se necesita ningun requerimiento por parte del usuario, solo se necesita
   un arreglo de 200 posiciones que ya se tiene definido.

   Garantiza : El programa genera  un arreglo de 200 letras mayúsculas aleatorias,
   imprima este arreglo y luego imprima cuantas veces se repite cada letra en el arreglo.*/

#include <iostream>
#include<time.h>
using namespace std;

int contador(char*, char, int);//Prototipo de funcion que va a contar cuantas veces esta la misma letra en todo el arreglo, los paramteros de entrada es el arreglo, la letra y un estero.


int main()
{
    srand(time(0));
    char letras[200]={};//Se declara el arreglo de 200 posiciones
    char *puntero=letras;//Se declara un puntero que va a facilitar moverse por arreglo
    int cont=0;

    for (int i=0; i<200; i++){//ciclo para recorrer las 200 posiciones del arreglo
        *(puntero+i)= 65+rand()%25;//instruccion que llena el arreglo con numeros aleatorios del 65 al 91, rango de las letras mayusculas.
       // cout << (*letras)+i << ",";

        for (int i=0; i<200; i++){//ciclo para recorrer las 200 posiciones del arreglo
            *(puntero+i)= 65+rand()%25;//instruccion que llena el arreglo con numeros aleatorios del 65 al 91, rango de las letras mayusculas.
           // cout << (*letras)+i << ",";

            for(int k=65; k<91; k++){//ciclo que recorre todas las letras mayusculas y hace una comparacion
                for(int j=0; j<200; j++){//ciclo que recorre todas las posiciones del arreglo

                    if (*(puntero+j)==k){//condicional de comparacion
                        cont = contador(puntero,*(puntero+j),200);//se llama a la funcion para que cuente cuantas veces esta la letra en el arreglo de 200.
                        cout<< *(puntero+j) << " : " << cont << endl;
                    }
                    cont=0;
                }

            }
        }
    }
    return 0;
}

int contador(char *puntero, char busqueda, int lim)//implementacion de la funcion
{
    int cont=0;
    for(int i =0; i<lim; i++)//ciclo que me cuenta cuantas veces esta la letra busqueda en el arreglo
    {
        if(*(puntero+i)==busqueda)//condicional que se mete a todas las posiciones del arreglo y su contenido lo compara con la letra buscada.
        {
            cont++;//contador que incrementa si la letra se encuentra
        }
    }
    return cont;//Retorna el numero de veces que la letra se encontro
}
